<?php
return [
    'translatable'=>false,
    'locale'=>config('app.locale'),
    'translatable_locales'=>['fr','en']
];