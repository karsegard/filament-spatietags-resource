<?php

namespace KDA\Filament\Resources;
use Filament\PluginServiceProvider;
use Spatie\LaravelPackageTools\Package;
use Livewire\Livewire;

class SpatieTagsResourceProvider extends PluginServiceProvider
{
    protected array $styles = [
    //    'my-package-styles' => __DIR__ . '/../dist/app.css',
    ];

    protected array $widgets = [
    //    CustomWidget::class,
    ];

    protected array $pages = [
    //    CustomPage::class,
    ];

    protected array $resources = [
   //     CustomResource::class,
        TagResource::class,
        
    ];

    protected function getResources(): array
    {
      //  return $this->resources;
      if(config('kda/filament-spatietags-resource.translatable') === true){
        return [
            TranslatableTagResource::class
        ];
      }
        return [
            TagResource::class
        ];
      
    }

    protected array $relationManagers = [
    ];
 
    public function configurePackage(Package $package): void
    {
        $package->name('filament-spatietags-resource')->hasConfigFile('kda/filament-spatietags-resource');
    }

    public function packageBooted(): void
    {
        parent::packageBooted();
        //Livewire::component('filament-media-manager-modal',\KDA\Filament\MediaManager\Livewire\MediaItemModal::class);
    }
}
