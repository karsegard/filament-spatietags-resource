<?php

namespace KDA\Filament\Resources\TranslatableTagResource\Pages;

use KDA\Filament\Resources\TagResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;
use KDA\Filament\Resources\TranslatableTagResource;

class ListTags extends ListRecords
{
    use ListRecords\Concerns\Translatable;
    protected static string $resource = TranslatableTagResource::class;

    protected function getActions(): array
    {
        return [
          //  Actions\CreateAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
