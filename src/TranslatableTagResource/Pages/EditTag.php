<?php

namespace KDA\Filament\Resources\TranslatableTagResource\Pages;

use KDA\Filament\Resources\TagResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\EditRecord;
use KDA\Filament\Resources\TranslatableTagResource;

class EditTag extends EditRecord
{
    use EditRecord\Concerns\Translatable;
    protected static string $resource = TranslatableTagResource::class;

    protected function getActions(): array
    {
        return [
            Actions\DeleteAction::make(),
            Actions\LocaleSwitcher::make(),
        ];
    }
}
