<?php

namespace KDA\Filament\Resources\TranslatableTagResource\Pages;

use KDA\Filament\Resources\TagResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;
use KDA\Filament\Resources\TranslatableTagResource;

class CreateTag extends CreateRecord
{
    use CreateRecord\Concerns\Translatable;
 
    protected static string $resource = TranslatableTagResource::class;
    protected function getActions(): array
    {
        return [
            Actions\LocaleSwitcher::make(),
        ];
    }
}
