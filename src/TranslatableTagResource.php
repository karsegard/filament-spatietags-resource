<?php

namespace KDA\Filament\Resources;

use KDA\Filament\Resources\TranslatableTagResource\Pages;
use KDA\Filament\Resources\TagResource\RelationManagers;
use Filament\Forms;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Spatie\Tags\Tag;
use Filament\Resources\Concerns\Translatable;
use Filament\Tables\Actions\DeleteAction;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Filters\SelectFilter;

class TranslatableTagResource extends TagResource
{
    use Translatable;
    
    public static function getTranslatableLocales(): array
    {
        return config('kda/filament-spatietags-resource.translatable_locales');
    }
    
    public static function form(Form $form): Form
    {
        $locale = config('kda/filament-spatietags-resource.locale');
        return $form
            ->schema([
                //
                TextInput::make("name")
            ]);
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTags::route('/'),
            'create' => Pages\CreateTag::route('/create'),
            'edit' => Pages\EditTag::route('/{record}/edit'),
        ];
    }    
}
