<?php

namespace KDA\Filament\Resources\TagResource\Pages;

use KDA\Filament\Resources\TagResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\CreateRecord;

class CreateTag extends CreateRecord
{
    protected static string $resource = TagResource::class;
 
}
