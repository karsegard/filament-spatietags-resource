<?php

namespace KDA\Filament\Resources\TagResource\Pages;

use KDA\Filament\Resources\TagResource;
use Filament\Pages\Actions;
use Filament\Resources\Pages\ListRecords;

class ListTags extends ListRecords
{
    protected static string $resource = TagResource::class;

    protected function getActions(): array
    {
        return [
        //    Actions\CreateAction::make(),
        ];
    }
}
