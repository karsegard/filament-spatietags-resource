<?php

namespace KDA\Filament\Resources;

use KDA\Filament\Resources\TagResource\Pages;
use KDA\Filament\Resources\TagResource\RelationManagers;
use Filament\Forms;
use Filament\Forms\Components\TextInput;
use Filament\Resources\Form;
use Filament\Resources\Resource;
use Filament\Resources\Table;
use Filament\Tables;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;
use Spatie\Tags\Tag;
use Filament\Resources\Concerns\Translatable;
use Filament\Tables\Actions\DeleteAction;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Filters\SelectFilter;

class TagResource extends Resource
{
    protected static ?string $model = Tag::class;
    protected static ?string $navigationIcon = 'heroicon-o-hashtag';

  
    public static function form(Form $form): Form
    {
        $locale = config('kda/filament-spatietags-resource.locale');
        return $form
            ->schema([
                //
                TextInput::make("name.{$locale}")->label('Name')
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                //
                TextColumn::make('name'),
                TextColumn::make('type'),
            ])
            ->filters([
                //
                SelectFilter::make('type')->options(fn()=>Tag::pluck('type','type')->unique())->query(function ($query,$data){
                    return $query->when(!empty($data['value']),fn ($q)=> $q->withType($data['value']));
                })
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
                DeleteAction::make()
            ])
            ->bulkActions([
                Tables\Actions\DeleteBulkAction::make(),
            ]);
    }
    
    public static function getRelations(): array
    {
        return [
            //
        ];
    }
    
    public static function getPages(): array
    {
        return [
            'index' => Pages\ListTags::route('/'),
            'create' => Pages\CreateTag::route('/create'),
            'edit' => Pages\EditTag::route('/{record}/edit'),
        ];
    }    
}
